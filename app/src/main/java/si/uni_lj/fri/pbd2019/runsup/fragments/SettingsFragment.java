package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import si.uni_lj.fri.pbd2019.runsup.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    private static final String TAG = SettingsFragment.class.getSimpleName();

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pref_main, rootKey);
    }
}
