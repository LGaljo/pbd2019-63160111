package si.uni_lj.fri.pbd2019.runsup.model.daos;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class WorkoutDao {
    private Dao<Workout, Long> workoutDao;

    public WorkoutDao(Dao<Workout, Long> workoutDao) {
        this.workoutDao = workoutDao;
    }

    public Workout getWorkout(Long id) {
        try {
            return workoutDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Workout> getAllWorkouts(boolean asc) {
        try {
            return workoutDao.queryBuilder()
                    .orderBy("created", asc)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Workout> getAllWorkoutsByUser(boolean asc, User user) {
        try {
            return workoutDao.queryBuilder()
                    .orderBy("created", asc)
                    .where()
                    .eq("user_id", user.getId().toString())
                    .and()
                    .ne("status", 3)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteWorkout(Long id) {
        try {
            workoutDao.delete(getWorkout(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteWorkout(Workout workout) {
        try {
            workoutDao.delete(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrUpdateWorkout(Workout workout) {
        try {
            workoutDao.createOrUpdate(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createWorkout(Workout workout) {
        try {
            workoutDao.create(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateWorkout(Workout workout) {
        try {
            workoutDao.update(workout);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllByUser(User user) {
        try {
            DeleteBuilder<Workout, Long> builder =  workoutDao.deleteBuilder();
            builder.where().eq("user_id", user.getId().toString());
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            DeleteBuilder<Workout, Long> builder =  workoutDao.deleteBuilder();
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
