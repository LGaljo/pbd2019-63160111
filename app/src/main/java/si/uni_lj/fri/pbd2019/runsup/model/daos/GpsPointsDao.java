package si.uni_lj.fri.pbd2019.runsup.model.daos;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class GpsPointsDao {
    private Dao<GpsPoint, Long> gpsPointDao;

    public GpsPointsDao(Dao<GpsPoint, Long> gpsPointDao) {
        this.gpsPointDao = gpsPointDao;
    }

    public GpsPoint getGpsPoint(Long id) {
        try {
            return gpsPointDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<GpsPoint> getGpsPointsByWorkout(Workout workout) {
        try {
            return gpsPointDao.queryBuilder()
                    .orderBy("created", true)
                    .where()
                    .eq("workout_id", workout.getId().toString())
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<GpsPoint> getAllGpsPoints() {
        try {
            return gpsPointDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteGpsPoint(Long id) {
        try {
            gpsPointDao.delete(getGpsPoint(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteGpsPoint(GpsPoint gpsPoint) {
        try {
            gpsPointDao.delete(gpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrUpdateGpsPoint(GpsPoint gpsPoint) {
        try {
            gpsPointDao.createOrUpdate(gpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createGpsPoint(GpsPoint gpsPoint) {
        try {
            gpsPointDao.create(gpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateGpsPoint(GpsPoint gpsPoint) {
        try {
            gpsPointDao.update(gpsPoint);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            DeleteBuilder<GpsPoint, Long> builder =  gpsPointDao.deleteBuilder();
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
