package si.uni_lj.fri.pbd2019.runsup;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.CYCLING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.RUNNING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.UNDEFINED;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.WALKING;

public class WorkoutDetailActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLoadedCallback {
    private static final String TAG = WorkoutDetailActivity.class.getSimpleName();

    private Long workoutId;

    private GoogleMap mMap;

    private Workout workout;
    private WorkoutDao workoutDao;
    private GpsPointsDao gpsPointsDao;

    private ArrayList<GpsPoint> gpsPointList = new ArrayList<>();

    private SharedPreferences settings;

    boolean preference_gps;
    int preference_units;
    private String units_dist;
    private String units_pace;

    @BindView(R.id.textview_workoutdetail_workouttitle)
    TextView textview_workoutdetail_workouttitle;
    @BindView(R.id.textview_workoutdetail_sportactivity)
    TextView textview_workoutdetail_sportactivity;
    @BindView(R.id.textview_workoutdetail_activitydate)
    TextView textview_workoutdetail_activitydate;
    @BindView(R.id.textview_workoutdetail_valueduration)
    TextView textview_workoutdetail_valueduration;
    @BindView(R.id.textview_workoutdetail_valuedistance)
    TextView textview_workoutdetail_valuedistance;
    @BindView(R.id.textview_workoutdetail_valuecalories)
    TextView textview_workoutdetail_valuecalories;
    @BindView(R.id.textview_workoutdetail_valueavgpace)
    TextView textview_workoutdetail_valueavgpace;

    @BindView(R.id.fragment_workoutdetail_map)
    View fragment_workoutdetail_map;
    @BindView(R.id.fragment_workoutdetail_map_container)
    FrameLayout fragment_workoutdetail_map_container;
    @BindView(R.id.workout_info)
    ConstraintLayout workout_info;
    @BindView(R.id.workout_summary)
    ConstraintLayout workout_summary;
    @BindView(R.id.workout_social)
    ConstraintLayout workout_social;
    @BindView(R.id.constraintlayout_workoutdetail_content)
    ConstraintLayout constraintlayout_workoutdetail_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout_detail);
        ButterKnife.bind(this);
        setTitle("Workout Review");

        manageUnits();
        getAndSetWorkoutData();

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (gpsPointList.size() < 2) {
            fragment_workoutdetail_map_container.setVisibility(View.GONE);
        } else {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.fragment_workoutdetail_map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        manageUnits();
        getAndSetWorkoutData();
    }

    private void manageUnits() {
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);
        preference_gps = settings.getBoolean(SettingsActivity.GPS, true);

        units_dist = getString(R.string.all_labeldistanceunitkilometers);
        units_pace = getString(R.string.all_min) + "/"
                        + getString(R.string.all_labeldistanceunitkilometers);
        if (preference_units == 1) {
            units_dist = getString(R.string.all_labeldistanceunitmiles);
            units_pace = getString(R.string.all_min) + "/"
                    + getString(R.string.all_labeldistanceunitmiles);
        }
    }

    @SuppressLint("SetTextI18n")
    private void getAndSetWorkoutData() {
        // Pridodi workout ID iz intenta
        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", 0);
        Log.d(TAG, "getAndSetWorkoutData: workout id: " + workoutId);

        // Iz baze pridobi workout
        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            workout = workoutDao.getWorkout(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // Preveri, ce workout obstaja
        if (workout == null) {
            Toast.makeText(this, "No current workout found", Toast.LENGTH_SHORT).show();
            return;
        }

        // Nastavi ime workouta
        textview_workoutdetail_workouttitle.setText(workout.getTitle());

        // Set activity name based on activity type from intent
        switch (workout.getSportActivity()) {
            case RUNNING:
                textview_workoutdetail_sportactivity.setText(getResources().getText(R.string.running));
                break;
            case WALKING:
                textview_workoutdetail_sportactivity.setText(getResources().getText(R.string.walking));
                break;
            case CYCLING:
                textview_workoutdetail_sportactivity.setText(getResources().getText(R.string.cycling));
                break;
            case UNDEFINED:
                textview_workoutdetail_sportactivity.setText(getResources().getText(R.string.unknown));
                break;
        }

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd. MMM yyyy HH:mm:ss");
        String formattedDate = df.format(workout.getCreated());

        textview_workoutdetail_activitydate.setText(formattedDate);
        textview_workoutdetail_valueduration.setText(MainHelper.formatDuration(workout.getDuration()));
        textview_workoutdetail_valuecalories.setText(MainHelper.formatCalories(workout.getTotalCalories()));

        if (preference_units == 0) {
            textview_workoutdetail_valuedistance.setText(MainHelper.formatDistance(workout.getDistance()) + " " + units_dist);
            textview_workoutdetail_valueavgpace.setText(MainHelper.formatPace(workout.getPaceAvg()) + " " + units_pace);

        } else {
            textview_workoutdetail_valuedistance.setText(MainHelper.formatDistance(MainHelper.kmToMi(workout.getDistance())) + " " + units_dist);
            textview_workoutdetail_valueavgpace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(workout.getPaceAvg())) + " " + units_pace);
        }

        // Pridobi GPS tocke tega workouta
        try {
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            gpsPointList = new ArrayList<>();
            gpsPointList.addAll(gpsPointsDao.getGpsPointsByWorkout(workout));
            Log.d(TAG, "getAndSetWorkoutData: Workout has this amount of gps points: " + gpsPointList.size());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.workout_title_container)
    public void editWorkoutTitle(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Get the layout inflater and set layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_workout_title, null);

        // Get EditText field
        EditText edit_title = dialogView.findViewById(R.id.title);
        edit_title.setText(textview_workoutdetail_workouttitle.getText());

        // Set view
        builder.setView(dialogView);

        // Add the buttons
        builder.setPositiveButton(R.string.ok, (DialogInterface dialog, int id) -> {
            // User clicked OK button
            dialog.cancel();
            textview_workoutdetail_workouttitle.setText(edit_title.getText().toString());
            workout.setTitle(edit_title.getText().toString());
            workoutDao.updateWorkout(workout);
        });

        builder.setNegativeButton(R.string.cancel, (DialogInterface dialog, int id) -> {
            // User clicked Cancel button
            dialog.dismiss();
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick({R.id.button_workoutdetail_twittershare, R.id.button_workoutdetail_fbsharebtn, R.id.button_workoutdetail_gplusshare})
    public void shareViaPlain() {
        String dist = MainHelper.formatDistance(workout.getDistance()) + " " + units_dist;
        if (preference_units == 1) {
            dist = MainHelper.formatDistance(MainHelper.kmToMi(workout.getDistance())) + " " + units_dist;
        }

        Intent shareIntent = new Intent(Intent.ACTION_SEND)
                .setType("text/plain")
                .putExtra(android.content.Intent.EXTRA_TEXT, String.format("I was out for %s. I did %s in %s.",
                        MainHelper.formatActivityType(this, workout.getSportActivity()),
                        dist + " " + units_dist, MainHelper.formatDuration(workout.getDuration())));
        startActivity(Intent.createChooser(shareIntent, "Share via"));
    }

    @OnClick(R.id.button_workoutdetail_emailshare)
    public void sendEmailIntent() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd. MMM yyyy");

        String dist = MainHelper.formatDistance(workout.getDistance()) + " " + units_dist;
        if (preference_units == 1) {
            dist = MainHelper.formatDistance(MainHelper.kmToMi(workout.getDistance())) + " " + units_dist;
        }

        Intent emailIntent = new Intent(Intent.ACTION_SEND)
                .setType("text/rfc822")
                .putExtra(Intent.EXTRA_SUBJECT, "Workout on " + df.format(workout.getCreated()))
                .putExtra(Intent.EXTRA_TEXT, String.format("I was out for %s. I did %s in %s.",
                        MainHelper.formatActivityType(this, workout.getSportActivity()),
                        dist, MainHelper.formatDuration(workout.getDuration())));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @OnClick(R.id.graph_button)
    public void graphs() {
        startActivity(new Intent(this, GraphActivity.class).putExtra("workoutId", workoutId));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLoadedCallback(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5), 6f));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.workoutdetail_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            Log.d(TAG, "Delete current workout with ID " + workout.getId() + ": " + workout.getTitle());
            workout.setStatus(Workout.statusDeleted);
            workoutDao.updateWorkout(workout);
            finish();
            return true;
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Intent openMap = new Intent(getApplicationContext(), MapsActivity.class);
        openMap.putExtra("workoutId", workoutId);
        startActivity(openMap);
    }

    @Override
    public void onMapLoaded() {
        if (!gpsPointList.isEmpty()) {
            MainHelper.createPolyLine(gpsPointList, mMap, true);

            if (gpsPointList.size() > 10) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (GpsPoint g : gpsPointList) {
                    builder.include(new LatLng(g.getLatitude(), g.getLongitude()));
                }
                LatLngBounds bounds = builder.build();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                mMap.animateCamera(cameraUpdate);
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gpsPointList.get(0).getLatitude(), gpsPointList.get(0).getLongitude()), 17));
            }
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5), 6f));
        }
    }
}
