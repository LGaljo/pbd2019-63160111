package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.enums.TooltipPositionMode;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

public class GraphActivity extends AppCompatActivity {
    private static final String TAG = GraphActivity.class.getSimpleName();
    private WorkoutDao workoutDao;
    private GpsPointsDao gpsPointsDao;
    private Long workoutId;
    private Workout workout;

    private List<DataEntry> dataSpeed = new ArrayList<>();
    private List<DataEntry> dataPace = new ArrayList<>();
    private List<DataEntry> dataHeight = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        AnyChartView chartSpeed = findViewById(R.id.chart_speed);
        AnyChartView chartPace = findViewById(R.id.chart_pace);
        AnyChartView chartHeight = findViewById(R.id.chart_height);

        fetchData();

        APIlib.getInstance().setActiveAnyChartView(chartSpeed);
        Cartesian cartesianSpeed = AnyChart.cartesian();
        cartesianSpeed.animation(true);
        cartesianSpeed.padding(10d, 10d, 15d, 10d);
        cartesianSpeed.crosshair().enabled(true);
        /*
        cartesianSpeed.crosshair()
                .yLabel(true)
                .yStroke((Stroke) null, null, null, (String) null, (String) null)
                .xLabel(true)
                .xStroke((Stroke) null, null, null, (String) null, (String) null);*/
        cartesianSpeed.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesianSpeed.title("Graph of speed");
        cartesianSpeed.yAxis(0).title(String.format("Speed (%s)", "km/h"));
        cartesianSpeed.xAxis(0).labels().padding(5d, 5d, 5d, 5d);
        cartesianSpeed.data(dataSpeed);
        chartSpeed.setChart(cartesianSpeed);



        APIlib.getInstance().setActiveAnyChartView(chartPace);
        Cartesian cartesianPace = AnyChart.cartesian();
        cartesianPace.animation(true);
        cartesianPace.padding(10d, 10d, 15d, 10d);
        cartesianPace.crosshair().enabled(true);
        /*
        cartesianPace.crosshair()
                .yLabel(true)
                .yStroke((Stroke) null, null, null, (String) null, (String) null)
                .xLabel(true)
                .xStroke((Stroke) null, null, null, (String) null, (String) null);*/
        cartesianPace.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesianPace.title("Graph of pace");
        cartesianPace.yAxis(0).title(String.format("Pace (%s)", "min/km"));
        cartesianPace.xAxis(0).labels().padding(5d, 5d, 5d, 5d);
        cartesianPace.data(dataPace);
        chartPace.setChart(cartesianPace);



        APIlib.getInstance().setActiveAnyChartView(chartHeight);
        Cartesian cartesianHeight = AnyChart.cartesian();
        cartesianHeight.animation(true);
        cartesianHeight.padding(10d, 10d, 15d, 10d);
        cartesianHeight.crosshair().enabled(true);
        /*
        cartesianHeight.crosshair()
                .yLabel(true)
                .yStroke((Stroke) null, null, null, (String) null, (String) null)
                .xLabel(true)
                .xStroke((Stroke) null, null, null, (String) null, (String) null);*/
        cartesianHeight.tooltip().positionMode(TooltipPositionMode.POINT);
        cartesianHeight.title("Graph of height");
        cartesianHeight.yAxis(0).title(String.format("Height (%s)", "meters"));
        cartesianHeight.xAxis(0).labels().padding(5d, 5d, 5d, 5d);
        cartesianHeight.data(dataHeight);
        chartHeight.setChart(cartesianHeight);
    }

    private void fetchData() {
        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", 0);
        Log.d(TAG, "getAndSetWorkoutData: workout id: " + workoutId);

        // Iz baze pridobi workout
        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            workout = workoutDao.getWorkout(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // Preveri, ce workout obstaja
        if (workout == null) {
            Toast.makeText(this, "No current workout found", Toast.LENGTH_SHORT).show();
        }

        // Nastavi ime workouta
        setTitle(workout.getTitle() + " Statistics");

        // Pridobi GPS tocke tega workouta
        try {
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            List<GpsPoint> gpsPointList = gpsPointsDao.getGpsPointsByWorkout(workout);
            double prevTime = gpsPointList.get(1).getCreated().getTime();
            for (int i = 2; i < gpsPointList.size() - 1; i++) {
                GpsPoint g = gpsPointList.get(i);
                double time = gpsPointList.get(i).getCreated().getTime();
                dataSpeed.add(new ValueDataEntry((time - prevTime)/1000, g.getSpeed()));
                dataPace.add(new ValueDataEntry((time - prevTime)/1000, g.getPace()));
                dataHeight.add(new ValueDataEntry((time - prevTime)/1000, g.getAltitude()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

}
