package si.uni_lj.fri.pbd2019.runsup.helpers;

import java.util.HashMap;
import java.util.List;

final public class SportActivities {
    private static final double runningMPHtoMET = 1.535353535;
    private static final double walkingMPHtoMET = 1.14;
    private static final double cyclingMPHtoMET = 0.744444444;

    public static final int UNDEFINED = -1;
    public static final int RUNNING = 0;
    public static final int WALKING = 1;
    public static final int CYCLING = 2;


    private static final HashMap<Integer, Double> cycling = new HashMap<Integer, Double>() {{
        put(10, 6.8);
        put(12, 8.0);
        put(14, 10.0);
        put(16, 12.8);
        put(18, 13.6);
        put(20, 15.8);
    }};
    private static final HashMap<Integer, Double> walking = new HashMap<Integer, Double>() {{
        put(1, 2.0);
        put(2, 2.8);
        put(3, 3.1);
        put(4, 3.5);
    }};
    private static final HashMap<Integer, Double> running = new HashMap<Integer, Double>() {{
        put(4, 6.0);
        put(5, 8.3);
        put(6, 9.8);
        put(7, 11.0);
        put(8, 11.8);
        put(9, 12.8);
        put(10, 14.5);
        put(11, 16.0);
        put(12, 19.0);
        put(13, 19.8);
        put(14, 23.0);
    }};

    /**
     * Returns MET value for an activity.
     * @param activityType - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param speed - speed in m/s
     * @return
     */
    public static double getMET(int activityType, Float speed) {
        int ceiled = (int)Math.ceil(MainHelper.mpsToMiph(speed));
        switch (activityType) {
            case RUNNING:
                // running
                if (running.containsKey(ceiled)) {
                    return running.get(ceiled);
                }
                return MainHelper.mpsToMiph(speed) * runningMPHtoMET;
            case WALKING:
                // walking
                if (walking.containsKey(ceiled)) {
                    return walking.get(ceiled);
                }
                return MainHelper.mpsToMiph(speed) * walkingMPHtoMET;
            case CYCLING:
                // cycling
                if (cycling.containsKey(ceiled)) {
                    return cycling.get(ceiled);
                }
                return MainHelper.mpsToMiph(speed) * cyclingMPHtoMET;
            default:
                return 0.0;
        }
    }

    public static float averageSpeed(List<Float> speedList) {
        float sum = 0;
        for (Float s : speedList) {
            sum += s;
        }
        return sum / speedList.size();
    }

    /**
     * Returns final calories computed from the data provided (returns value in kcal)
     * @param sportActivity - sport activity type (0 - running, 1 - walking, 2 - cycling)
     * @param weight - weight in kg
     * @param speedList - list of all speed values recorded (unit = m/s)
     * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
     * @return
     */
    public static double countCalories(int sportActivity, float weight, List<Float> speedList, double timeFillingSpeedListInHours) {
        float avgSpeed = (float)Math.ceil(averageSpeed(speedList));
        double met = getMET(sportActivity, avgSpeed);
        return met * weight * timeFillingSpeedListInHours;
    }
}
