package si.uni_lj.fri.pbd2019.runsup.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.App;
import si.uni_lj.fri.pbd2019.runsup.MainActivity;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.SettingsActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.CYCLING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.RUNNING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.UNDEFINED;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.WALKING;

public class TrackerService extends Service {
    private static final String TAG = TrackerService.class.getSimpleName();

    private static boolean[] vibrateList = new boolean[400];
    private ArrayList<Float> speedList = new ArrayList<>();
    private ArrayList<Location> posList = new ArrayList<>();
    private ArrayList<Double> paceList = new ArrayList<>();

    private Date last_update_time;
    private double altitude;
    private double calories;
    private Double distance;
    private Double pace;
    private int preference_units;
    private int state = STATE_STOPPED;
    private int type = UNDEFINED;
    private long duration;
    private Float speed;

    private final IBinder mBinder = new LocalBinder();

    private FusedLocationProviderClient mFusedLocationProviderClient;
    private GpsPointsDao gpsPointsDao;

    private Location mCurrentLocation;
    private Location mPrevLocation;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest = null;

    private Notification notification;
    private NotificationManagerCompat notificationManager;

    private SharedPreferences settings;

    private static final int VIBRATION_LENGTH = 2000;
    private static final Integer NOTIFICATION_ID = 12;
    private static final String CHANNEL_ID = "CurrentWorkoutChannel";
    private static Handler handler = new Handler();

    private static int session = 0;

    private Vibrator vibrator;

    private Workout workout;
    private WorkoutDao workoutDao;

    public static final int STATE_CONTINUE = 3;
    public static final int STATE_PAUSED = 1;
    public static final int STATE_RUNNING = 2;
    public static final int STATE_STOPPED = 0;

    public static final String COMMAND_CONTINUE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE";
    public static final String COMMAND_PAUSE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE";
    public static final String COMMAND_START = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START";
    public static final String COMMAND_STOP = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP";
    public static final String TICK = "si.uni_lj.fri.pbd2019.runsup.TICK";
    public static final String UPDATE_SPORT_ACTIVITY = "si.uni_lj.fri.pbd2019.runsup.UPDATE_SPORT_ACTIVITY";

    public TrackerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public TrackerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Arrays.fill(vibrateList, false);
        Log.d(TAG, "Start service");
        mFusedLocationProviderClient = new FusedLocationProviderClient(this);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);

        createNotificationChannel();
        notificationManager = NotificationManagerCompat.from(this);
        createNotification();

        this.startForeground(NOTIFICATION_ID, notification);
        createLocationRequest();
        createLocationCallback();

        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        workout = new Workout();

        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Disable location updates
        Log.d(TAG, "Stop TrackerService");
        this.stopLocationUpdates();
        this.stopForeground(true);
        this.stopSelf();
        handler.removeCallbacks(r);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        String action = null;
        try {
            action = intent.getAction();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Service received command " + action);

        if (action != null) {
            switch (action) {
                case COMMAND_START:
                    startLocationUpdates();
                    type = intent.getIntExtra("sportActivity", type);
                    state = STATE_RUNNING;
                    last_update_time = new Date();
                    manageUserWorkout();
                    handler.postDelayed(r, 1000);
                    Arrays.fill(vibrateList, false);
                    break;
                case COMMAND_PAUSE:
                    pauseWorkout();
                    sendTick();
                    break;
                case COMMAND_CONTINUE:
                    continueWorkout();
                    sendTick();
                    break;
                case COMMAND_STOP:
                    stopWorkout();
                    break;
                case UPDATE_SPORT_ACTIVITY:
                    type = intent.getIntExtra("sportActivity", type);
                    try {
                        if (workout != null) {
                            workout.setSportActivity(type);
                            workoutDao.updateWorkout(workout);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }

        return START_STICKY;
    }

    private void stopWorkout() {
        state = STATE_STOPPED;
        try {
            workout.setStatus(Workout.statusEnded);
            double tmp = 0;
            for (double f : paceList) {
                tmp += f;
            }
            workout.setPaceAvg(tmp / paceList.size());
            workoutDao.updateWorkout(workout);
            workout = new Workout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        notificationManager.cancel(NOTIFICATION_ID);
        stopLocationUpdates();
    }

    private void pauseWorkout() {
        session += 1;
        try {
            workout.setStatus(Workout.statusPaused);
            workoutDao.updateWorkout(workout);
        } catch (Exception e) {
            e.printStackTrace();
        }
        state = STATE_PAUSED;
    }

    private void continueWorkout() {
        state = STATE_CONTINUE;
        try {
            workout.setStatus(Workout.statusUnknown);
            workoutDao.updateWorkout(workout);
        } catch (Exception e) {
            e.printStackTrace();
        }
        last_update_time = new Date();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createNotification() {
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("fromNotification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        PendingIntent pausePendingIntent;
        NotificationCompat.Builder builder;
        builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        if (state == STATE_PAUSED) {
            Intent pauseIntent = new Intent(this, TrackerService.class);
            pauseIntent.setAction(TrackerService.COMMAND_CONTINUE);
            pausePendingIntent = PendingIntent.getForegroundService(this, 0, pauseIntent, 0);
            builder.addAction(R.drawable.ic_chart, getString(R.string.continueworkout), pausePendingIntent);
        } else if (state == STATE_RUNNING) {
            Intent pauseIntent = new Intent(this, TrackerService.class);
            pauseIntent.setAction(TrackerService.COMMAND_PAUSE);
            pausePendingIntent = PendingIntent.getForegroundService(this, 0, pauseIntent, 0);
            builder.addAction(R.drawable.ic_chart, getString(R.string.pause), pausePendingIntent);
        }

        switch (type) {
            case WALKING:
                builder.setSmallIcon(R.drawable.ic_directions_walk);
                builder.setContentTitle(getString(R.string.walking));
                break;
            case RUNNING:
                builder.setSmallIcon(R.drawable.ic_directions_run);
                builder.setContentTitle(getString(R.string.running));
                break;
            case CYCLING:
                builder.setSmallIcon(R.drawable.ic_directions_bike);
                builder.setContentTitle(getString(R.string.cycling));
                break;
            case UNDEFINED:
                builder.setSmallIcon(R.drawable.ic_error);
                builder.setContentTitle("No workout started");
                break;
        }

        if (state != STATE_STOPPED) {
            if (preference_units == 0) {
                builder.setContentText("Duration: " + MainHelper.formatDuration(duration / 1000) +
                        ", distance: " + MainHelper.formatDistance(distance) + " " + getString(R.string.all_labeldistanceunitkilometers));
            } else {
                builder.setContentText("Duration: " + MainHelper.formatDuration(duration / 1000) +
                        ", distance: " + MainHelper.formatDistance(MainHelper.kmToMi(distance)) + " " + getString(R.string.all_labeldistanceunitmiles));
            }
        }

        //builder.setStyle(new NotificationCompat.BigTextStyle().bigText("Much longer text that cannot fit one line..."));
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setOnlyAlertOnce(true);

        notification = builder.build();
    }

    private void manageUserWorkout() {
        List<Workout> w = workoutDao.getAllWorkoutsByUser(true, App.user);

        if (!w.isEmpty() && (w.get(w.size() - 1).getStatus() == Workout.statusPaused || w.get(w.size() - 1).getStatus() == Workout.statusUnknown)) {
            Log.d(TAG, "manageUserWorkout: restore workout " + w.get(w.size() - 1).getId() + " status " + w.get(w.size() - 1).getStatus());
            workout = new Workout();
            restoreWorkout(w.get(w.size() - 1));
        } else {
            startNewWorkout(w);
        }
    }

    private void restoreWorkout(Workout old) {
        ArrayList<GpsPoint> gpsPointsList = (ArrayList<GpsPoint>) gpsPointsDao.getGpsPointsByWorkout(old);
        workout = old;

        session = (int) gpsPointsList.get(gpsPointsList.size() - 1).getSessionNumber();
        duration = old.getDuration() * 1000;
        distance = old.getDistance();
        calories = old.getTotalCalories();
        pace = old.getPaceAvg();
        last_update_time = new Date();
        altitude = gpsPointsList.get(gpsPointsList.size() - 1).getAltitude();
        speed = gpsPointsList.get(gpsPointsList.size() - 1).getSpeed();

        for (GpsPoint g : gpsPointsList) {
            Location tmp = new Location("");
            tmp.setLatitude(g.getLatitude());
            tmp.setLongitude(g.getLongitude());
            posList.add(tmp);
            speedList.add(g.getSpeed());
        }
    }

    private void startNewWorkout(List<Workout> w) {
        Log.d(TAG, "start new workout");
        session = 0;
        duration = 0;
        distance = 0.0;
        calories = 0;
        pace = 0.0;
        altitude = 0;
        speed = 0f;
        posList = new ArrayList<>();
        speedList = new ArrayList<>();
        last_update_time = new Date();

        try {
            workout = new Workout();
            workout.setStatus(Workout.statusUnknown);
            workout.setTitle("Workout " + (w.size() + 1));
            workout.setSportActivity(type);
            workout.setDistance(0.0);
            workout.setDuration(0);
            workout.setPaceAvg(0);
            workout.setUser(App.user);
            workout.setTotalCalories(0.0);
            workoutDao.createWorkout(workout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveToDatabase(boolean saveLoc) {
        if (saveLoc) {
            try {
                GpsPoint tmp = new GpsPoint();
                tmp.setDuration(duration / 1000);
                tmp.setPace(pace);
                tmp.setAltitude(altitude);
                tmp.setWorkout(workout);
                tmp.setTotalCalories(calories);
                tmp.setLatitude(mCurrentLocation.getLatitude());
                tmp.setLongitude(mCurrentLocation.getLongitude());
                tmp.setSpeed(speed);
                tmp.setSessionNumber(session);
                gpsPointsDao.createGpsPoint(tmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            workout.setDistance(distance);
            workout.setDuration(duration / 1000);
            workout.setPaceAvg(pace);
            workout.setTotalCalories(calories);
            workout.setStatus(state);
            workoutDao.updateWorkout(workout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final Runnable r = () -> {
        if (state == STATE_RUNNING || state == STATE_CONTINUE) {
            sendTick();
        }
        handler.postDelayed(this.r, 1000);
    };

    private void sendTick() {
        Date current_time = new Date();
        duration += current_time.getTime() - last_update_time.getTime();
        last_update_time = current_time;

        saveToDatabase(false);

        try {
            if (App.userProfile != null && speedList.size() >= 2) {
                calories = SportActivities.countCalories(type, (float) App.userProfile.getWeight(), speedList, duration / 3600000f);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (App.userProfile == null) {
            Log.d(TAG, "userProfile is null");
        }

        if (speed.isNaN()) {
            speed = 0f;
        }
        if (pace.isNaN()) {
            pace = 0.0;
        }

        Intent intent = new Intent(TICK)
                .putExtra("workoutId", workout.getId())
                .putExtra("duration", duration / 1000)
                .putExtra("distance", distance)
                .putExtra("pace", pace)
                .putExtra("altitude", altitude)
                .putExtra("speed", speed)
                .putExtra("calories", calories)
                .putExtra("state", state)
                .putExtra("sportActivity", type)
                .putExtra("location", mCurrentLocation);

        sendBroadcast(intent);
        mCurrentLocation = null;
        createNotification();
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(NOTIFICATION_ID, notification);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest()
                .setInterval(3000)
                .setSmallestDisplacement(10)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (state == STATE_RUNNING || state == STATE_CONTINUE) {
                    mCurrentLocation = locationResult.getLastLocation();

                    // After a location is received, it is stored in the database.
                    // At the same time the workout is also stored (updated) in the database.
                    saveToDatabase(true);

                    if (!posList.isEmpty()) {
                        double dist_diff = posList.get(posList.size() - 1).distanceTo(mCurrentLocation);
                        distance += dist_diff;
                        calcPace();
                    }

                    float currentSpeed = 0;
                    if (mPrevLocation != null) {
                        currentSpeed = (float) (mPrevLocation.distanceTo(mCurrentLocation) / ((mCurrentLocation.getTime() - mPrevLocation.getTime()) / 1000.0));
                    }
                    mPrevLocation = mCurrentLocation;

                    calcSpeed();
                    manageVibrator();
                    altitude = mCurrentLocation.getAltitude();
                    //speed = mCurrentLocation.getSpeed();
                    speedList.add(currentSpeed);
                    posList.add(mCurrentLocation);
                }
            }
        };
    }

    private void calcSpeed() {
        float tmp = 0;
        if (!posList.isEmpty() && !speedList.isEmpty() && speedList.size() > 3) {
            for (int i = speedList.size() - 1; i > speedList.size() - 4; i--) {
                tmp += speedList.get(i);
            }
            speed = tmp / 3;
        } else {
            for (float s : speedList) {
                tmp += s;
            }
            speed = tmp / speedList.size();
        }
        if (speed.isNaN()) {
            speed = 0f;
        }
    }

    private void manageVibrator() {
        if (preference_units == 0) {
            // If distance is between x970 and x030, then check if vibration is needed
            if (Math.abs(distance % 1000) < 30 && distance > 1000) {
                if (!vibrateList[(int) Math.floor(distance / 1000)]) {
                    System.out.println("vibrate");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibrator.vibrate(VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        vibrator.vibrate(200);
                    }
                    vibrateList[(int) Math.floor(distance / 1000)] = true;
                }
            }
        } else {
            double distInFeets = MainHelper.metersToFeet(distance);
            if (Math.abs(distInFeets % 5280) < 150 && distInFeets > 5280) {
                if (!vibrateList[(int) Math.floor(distInFeets / 5280)]) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibrator.vibrate(VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        vibrator.vibrate(200);
                    }
                    vibrateList[(int) Math.floor(distInFeets / 5280)] = true;
                }
            }
        }
    }

    private void calcPace() {
        double dist = 0;
        double dur = 0;
        Location prevLoc;
        if (!posList.isEmpty() && posList.size() > 3) {
            prevLoc = posList.get(posList.size() - 1);
            for (int i = posList.size() - 1; i >= 0 && i > posList.size() - 6; i--) {
                Location l = posList.get(i);
                dist += prevLoc.distanceTo(l);
                dur += prevLoc.getTime() - l.getTime();
                prevLoc = l;
            }
            Log.d(TAG, "calcPace: distance " + dist + "dur: " + dur);
            if (pace.isNaN() || dur < 1) {
                paceList.add(0.0);
            } else {
                pace = (dur / 60f) / dist;
                paceList.add(pace);
            }
        } else {
            pace = 0.0;
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        }
    }

    private void stopLocationUpdates() {
        try {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
