package si.uni_lj.fri.pbd2019.runsup.model.daos;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;

public class UserProfileDao {
    private Dao<UserProfile, Long> userProfileDao;

    public UserProfileDao(Dao<UserProfile, Long> userProfileDao) {
        this.userProfileDao = userProfileDao;
    }

    public UserProfile getUserProfile(Long id) {
        try {
            return userProfileDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UserProfile> getUserProfile(User user) {
        try {
            return userProfileDao.queryBuilder()
                    .where()
                    .eq("user_id", user.getId())
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UserProfile> getAllUserProfiles() {
        try {
            return userProfileDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteUserProfile(Long id) {
        try {
            userProfileDao.delete(getUserProfile(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserProfile(UserProfile userProfile) {
        try {
            userProfileDao.delete(userProfile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrUpdateUserProfile(UserProfile userProfile) {
        try {
            userProfileDao.createOrUpdate(userProfile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createUserProfile(UserProfile userProfile) {
        try {
            userProfileDao.create(userProfile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUserProfile(UserProfile userProfile) {
        try {
            userProfileDao.update(userProfile);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            DeleteBuilder<UserProfile, Long> builder =  userProfileDao.deleteBuilder();
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
