package si.uni_lj.fri.pbd2019.runsup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

public class ActiveWorkoutMapActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, GoogleMap.OnCameraMoveStartedListener {
    private static final String TAG = ActiveWorkoutMapActivity.class.getSimpleName();

    private BroadcastReceiver mBroadcastReceiver;

    private boolean recentre = true;

    private GoogleMap mMap;
    private Long workoutId;
    private Workout workout;
    private WorkoutDao workoutDao;
    private GpsPointsDao gpsPointsDao;
    private ArrayList<LatLng> gpsPointList = new ArrayList<>();
    private ArrayList<GpsPoint> selfGPSPointList;

    private Polyline polyline;
    private Circle circle;

    private LatLng latLng_last;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_workout_map);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", 1L);
        Log.d(TAG, "onCreate: workout id I got " + workoutId);
        pridobiPodatke();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_activeworkoutmap_map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != null && intent.getAction().equals(TrackerService.TICK)) {
                    try {
                        Bundle b = intent.getExtras();
                        if (b != null) {
                            Location l = (Location) b.get("location");
                            if (l != null) {
                                LatLng latLng_current = new LatLng(l.getLatitude(), l.getLongitude());
                                gpsPointList.add(latLng_current);
                                Log.d(TAG, "arraylist " + gpsPointList.size());
                                polyline.setPoints(gpsPointList);
                                Log.d(TAG, "onReceive: " + polyline);

                                if (recentre) {
                                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng_current, 16);
                                    mMap.animateCamera(cameraUpdate);
                                    Log.d(TAG, "broadcast: recentre now true");
                                    recentre = true;
                                }

                                if (circle != null) {
                                    circle.remove();
                                }

                                circle = mMap.addCircle(new CircleOptions()
                                        .center(latLng_current)
                                        .radius(3)
                                        .fillColor(0x220000FF)
                                        .strokeColor(Color.BLUE)
                                );
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        this.registerReceiver(mBroadcastReceiver, new IntentFilter(TrackerService.TICK));
    }

    @OnClick(R.id.button_activeworkoutmap_recentre)
    public void recenterMap() {
        if (latLng_last != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng_last, 16);
            mMap.animateCamera(cameraUpdate);
        }
        Log.d(TAG, "recenterMap: recentre true");
        recentre = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(mBroadcastReceiver);
    }

    private void pridobiPodatke() {
        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            workout = workoutDao.getWorkout(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (workout == null) {
            Toast.makeText(this, "No current workout found", Toast.LENGTH_SHORT).show();
            return;
        }
        gpsPointList = new ArrayList<>();
        gpsPointList.clear();
        ArrayList<GpsPoint> list = (ArrayList<GpsPoint>) gpsPointsDao.getGpsPointsByWorkout(workout);
        selfGPSPointList = list;
        for (GpsPoint g : list) {
            gpsPointList.add(new LatLng(g.getLatitude(), g.getLongitude()));
        }
        if (gpsPointList.size() > 1) {
            latLng_last = gpsPointList.get(gpsPointList.size() - 1);
        }
    }

    @OnClick(R.id.button_activeworkoutmap_back)
    public void returnToStopwatchFragment() {
        finish();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.8), 7));
    }

    @Override
    public void onMapLoaded() {
        if (gpsPointList != null && !gpsPointList.isEmpty()) {
            LatLng first = new LatLng(gpsPointList.get(0).latitude, gpsPointList.get(0).longitude);
            LatLng last = new LatLng(gpsPointList.get(gpsPointList.size() - 1).latitude,
                    gpsPointList.get(gpsPointList.size() - 1).longitude);

            circle = mMap.addCircle(new CircleOptions()
                            .center(last)
                            .radius(3)
                            .fillColor(0x220000FF)
                            .strokeColor(Color.BLUE));

            polyline = MainHelper.createPolyLine(selfGPSPointList, mMap, false);
            Log.d(TAG, "onMapLoaded: recentre true");
            recentre = true;
        }
    }

    @Override
    public void onCameraMoveStarted(int i) {
        Log.d(TAG, "onCameraMoveStarted: recentre false");
        if (REASON_GESTURE == i) {
            recentre = false;
        }
    }
}
