package si.uni_lj.fri.pbd2019.runsup.model;

import android.location.Location;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "GpsPoint")
public class GpsPoint {
    @DatabaseField(generatedId = true, canBeNull = false)
    private Long id;
    @DatabaseField(canBeNull = false, foreign = true)
    private Workout workout;
    @DatabaseField()
    private Long sessionNumber;
    @DatabaseField()
    private double latitude;
    @DatabaseField()
    private double longitude;
    @DatabaseField()
    private long duration;
    @DatabaseField()
    private float speed;
    @DatabaseField()
    private double pace;
    @DatabaseField()
    private double altitude;
    @DatabaseField()
    private double totalCalories;
    @DatabaseField(version = true, format = "yyyy-MM-dd HH:mm:ss", dataType = DataType.DATE, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL", readOnly = true, canBeNull = false)
    private Date created;
    @DatabaseField(version = true, format = "yyyy-MM-dd HH:mm:ss", dataType = DataType.DATE)
    private Date lastUpdate;

    public GpsPoint() {
    }

    public GpsPoint(Workout workout, Long sessionNumber, Location location, long duration, float speed, double pace, double altitude, double totalCalories) {
        this.workout = workout;
        this.sessionNumber = sessionNumber;
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.duration = duration;
        this.speed = speed;
        this.pace = pace;
        this.altitude = altitude;
        this.totalCalories = totalCalories;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Workout getWorkout() {
        return workout;
    }
    public void setWorkout(Workout workout) {
        this.workout = workout;
    }
    public long getSessionNumber() {
        return sessionNumber;
    }
    public void setSessionNumber(long sessionNumber) {
        this.sessionNumber = sessionNumber;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public long getDuration() {
        return duration;
    }
    public void setDuration(long duration) {
        this.duration = duration;
    }
    public float getSpeed() {
        return speed;
    }
    public void setSpeed(float speed) {
        this.speed = speed;
    }
    public double getPace() {
        return pace;
    }
    public void setPace(double pace) {
        this.pace = pace;
    }
    public double getAltitude() {
        return altitude;
    }
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
    public double getTotalCalories() {
        return totalCalories;
    }
    public void setTotalCalories(double totalCalories) {
        this.totalCalories = totalCalories;
    }
    public Date getCreated() {
        return created;
    }
    public void setCreated(Date created) {
        this.created = created;
    }
    public Date getLastUpdate() {
        return lastUpdate;
    }
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
