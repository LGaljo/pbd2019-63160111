package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import si.uni_lj.fri.pbd2019.runsup.App;
import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.SettingsActivity;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncData;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

import static si.uni_lj.fri.pbd2019.runsup.App.user;

public class HistoryFragment extends Fragment {
    private static final String TAG = HistoryFragment.class.getSimpleName();

    @BindView(R.id.listview_history_workouts)
    ListView lv;
    @BindView(R.id.textview_history_noHistoryData)
    TextView textview_history_noHistoryData;
    @BindView(R.id.swiperefreshlayout_history_workouts)
    SwipeRefreshLayout swipeContainer;

    private ArrayList<Workout> workoutArrayList = new ArrayList<>();

    SharedPreferences sharedPref;
    private HistoryListAdapter adapter;

    private WorkoutDao workoutDao;

    private boolean userSignedIn;

    private Handler mHandler = new Handler();

    final Runnable r = () -> {
        getWorkoutList(true);
        mHandler.postDelayed(this.r, 3000);
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getActivity().getSharedPreferences("runsup", Context.MODE_PRIVATE);
        userSignedIn = sharedPref.getBoolean("userSignedIn", false);

        mHandler.postDelayed(r, 3000);

        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);

        getWorkoutList(false);
        manageSwipeContainer();

        adapter = new HistoryListAdapter(getContext(), workoutArrayList);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((AdapterView<?> parent, View viewItem, int position, long id) -> {
            try {
                openDetailedView(viewItem, workoutArrayList.get(position).getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getWorkoutList(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.clear();
        menu.add(0, 0, Menu.NONE, getString(R.string.action_settings))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        menu.add(0, 1, Menu.NONE, getString(R.string.historysettings_actionclearhistory))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        if (userSignedIn) {
            menu.add(0, 2, Menu.NONE, getString(R.string.historysettings_actionsync))
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 0) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == 1) {
            showDialogClearHistory();
        } else if (item.getItemId() == 2) {
            syncData();
        }

        return super.onOptionsItemSelected(item);
    }

    private void getWorkoutList(boolean swapItems) {
        List<Workout> workoutList = workoutDao.getAllWorkoutsByUser(false, user);

        if (workoutArrayList == null) {
            workoutArrayList = new ArrayList<>();
        }
        if (workoutList != null) {
            workoutArrayList.clear();
            workoutArrayList.addAll(workoutList);
        }

        if (swapItems) {
            adapter.swapItems(workoutArrayList, swipeContainer);
        }

        if (!workoutArrayList.isEmpty()) {
            textview_history_noHistoryData.setVisibility(View.INVISIBLE);
        }
    }

    private void showDialogClearHistory() {
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_message_delete_workouts)
                .setPositiveButton(R.string.yes, (DialogInterface dialog, int which) -> {
                    ArrayList<Workout> list = (ArrayList<Workout>) workoutDao.getAllWorkoutsByUser(true, user);
                    try {
                        for (Workout w : list) {
                            w.setStatus(Workout.statusDeleted);
                            workoutDao.updateWorkout(w);
                        }
                        getWorkoutList(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "History cleared", Snackbar.LENGTH_LONG).setAction("No action", null).show();
                    getActivity().runOnUiThread(() -> {
                        textview_history_noHistoryData.setVisibility(View.VISIBLE);
                    });
                })
                .setNegativeButton(R.string.no, (DialogInterface dialog, int which) -> {
                    // Close dialog and do nothing
                    dialog.dismiss();
                })
                .show();
    }

    private void manageSwipeContainer() {
        swipeContainer.setOnRefreshListener(() -> getActivity().runOnUiThread(() -> {
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Wait, syncing for a few seconds", Snackbar.LENGTH_LONG).setAction("No action", null).show();
            Thread thread = new Thread(SyncData::sync);
            thread.start();
        }));
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    public void openDetailedView(View view, Long workoutId) {
        Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
        intent.putExtra("workoutId", workoutId);
        startActivity(intent);
    }

    private void syncData() {
        Snackbar.make(getActivity().findViewById(android.R.id.content), "Wait, syncing for a few seconds", Snackbar.LENGTH_LONG).setAction("No action", null).show();
        Thread thread = new Thread(SyncData::sync);
        thread.start();
    }
}