package si.uni_lj.fri.pbd2019.runsup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;

public class HistoryListAdapter extends ArrayAdapter<Workout> {
    private Context context;
    private ArrayList<Workout> workoutArrayList = new ArrayList<>();

    private SharedPreferences settings;
    private int preference_units;

    public HistoryListAdapter(@NonNull Context context, ArrayList<Workout> workoutArrayList) {
        super(context, 0, workoutArrayList);
        this.context = context;
        this.workoutArrayList = workoutArrayList;

        settings = PreferenceManager.getDefaultSharedPreferences(context);
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);
    }

    private String formatWorkoutInfo(Workout workout) {
        String units_dist = getContext().getString(R.string.all_labeldistanceunitkilometers);
        String units_pace =
                getContext().getString(R.string.all_min) + "/"
                + getContext().getString(R.string.all_labeldistanceunitkilometers);
        if (preference_units == 1) {
            units_dist = getContext().getString(R.string.all_labeldistanceunitmiles);
            units_pace = getContext().getString(R.string.all_min) + "/"
                            + getContext().getString(R.string.all_labeldistanceunitmiles);
        }
        return new StringBuilder()
                .append(MainHelper.formatDuration(workout.getDuration()))
                .append(" ")
                .append(MainHelper.formatActivityType(getContext(), workout.getSportActivity()))
                .append(" | ")
                .append(MainHelper.formatDistance(workout.getDistance())).append(" ").append(units_dist)
                .append(" | ")
                .append(MainHelper.formatCalories(workout.getTotalCalories()))
                .append(" | avg ")
                .append(MainHelper.formatPace(workout.getPaceAvg())).append(" ").append(units_pace)
                .toString();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Workout workout = workoutArrayList.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_history, parent, false);
        }

        ImageView history_icon = (ImageView) convertView.findViewById(R.id.imageview_history_icon);
        TextView history_title = (TextView) convertView.findViewById(R.id.textview_history_title);
        TextView history_datetime = (TextView) convertView.findViewById(R.id.textview_history_datetime);
        TextView history_history_sportactivity = (TextView) convertView.findViewById(R.id.textview_history_sportactivity);

        if (workout != null) {
            history_title.setText(workout.getTitle());

            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd. MMM yyyy HH:mm:ss");
            String formattedDate = df.format(workout.getCreated());
            history_datetime.setText(formattedDate);

            history_history_sportactivity.setText(formatWorkoutInfo(workout));

            switch (workout.getSportActivity()) {
                case SportActivities.WALKING:
                    history_icon.setImageResource(R.drawable.ic_directions_walk);
                    break;
                case SportActivities.RUNNING:
                    history_icon.setImageResource(R.drawable.ic_directions_run);
                    break;
                case SportActivities.CYCLING:
                    history_icon.setImageResource(R.drawable.ic_directions_bike);
                    break;
                case SportActivities.UNDEFINED:
                    history_icon.setImageResource(R.drawable.ic_error);
                    break;
            }
        }

        return convertView;
    }

    public void swapItems(ArrayList<Workout> workoutArrayList, SwipeRefreshLayout swipeRefreshLayout) {
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);
        this.workoutArrayList = workoutArrayList;
        notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }
}
