package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.SettingsActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncData;

public class AboutFragment extends Fragment {
    private static final String TAG = AboutFragment.class.getSimpleName();

    SharedPreferences sharedPref;

    private boolean userSignedIn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = getActivity().getSharedPreferences("runsup", Context.MODE_PRIVATE);
        String display_name = sharedPref.getString("user_displayname", null);
        userSignedIn = sharedPref.getBoolean("userSignedIn", false);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.clear();
        menu.add(0, 0, Menu.NONE, getString(R.string.action_settings))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        if (userSignedIn) {
            menu.add(0, 1, Menu.NONE, getString(R.string.historysettings_actionsync))
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            Thread thread = new Thread(() -> {
                getActivity().runOnUiThread(() -> {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Syncing in background", Snackbar.LENGTH_LONG).setAction("No action", null).show();
                });
                SyncData.sync();
            });
            thread.start();
        } else if (item.getItemId() == 0) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
