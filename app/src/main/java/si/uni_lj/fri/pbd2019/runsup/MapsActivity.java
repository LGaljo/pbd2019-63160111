package si.uni_lj.fri.pbd2019.runsup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.sql.SQLException;
import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {
    private static final String TAG = MapsActivity.class.getSimpleName();

    private GoogleMap mMap;

    private Long workoutId;
    private Workout workout;
    private WorkoutDao workoutDao;

    private GpsPointsDao gpsPointsDao;
    private ArrayList<GpsPoint> gpsPointList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        getAndSetWorkoutData();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_maps_workoutmap);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5), 6f));
    }

    private void getAndSetWorkoutData() {
        // Pridodi workout ID iz intenta
        Intent intent = getIntent();
        workoutId = intent.getLongExtra("workoutId", 0);
        Log.d(TAG, "getAndSetWorkoutData: workout id: " + workoutId);

        // Iz baze pridobi workout
        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            workout = workoutDao.getWorkout(workoutId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // Preveri, ce workout obstaja
        if (workout == null) {
            Toast.makeText(this, "No current workout found", Toast.LENGTH_SHORT).show();
            return;
        }

        // Nastavi ime workouta
        setTitle(workout.getTitle());

        // Pridobi GPS tocke tega workouta
        try {
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            gpsPointList = new ArrayList<>();
            gpsPointList.clear();
            gpsPointList.addAll(gpsPointsDao.getGpsPointsByWorkout(workout));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapLoaded() {

        if (!gpsPointList.isEmpty()) {
            MainHelper.createPolyLineWithLabels(this, gpsPointList, mMap);

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (GpsPoint g : gpsPointList) {
                builder.include(new LatLng(g.getLatitude(), g.getLongitude()));
            }
            LatLngBounds bounds = builder.build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 100);
            mMap.animateCamera(cameraUpdate);
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(46, 14.5), 6f));
        }
    }
}
