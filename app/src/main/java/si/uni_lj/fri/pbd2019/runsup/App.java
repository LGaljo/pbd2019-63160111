package si.uni_lj.fri.pbd2019.runsup;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseUser;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.User;
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile;
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper;
import si.uni_lj.fri.pbd2019.runsup.model.daos.UserDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.UserProfileDao;

public class App extends Application {
    private static final String TAG = App.class.getSimpleName();

    public static DatabaseHelper databaseHelper;
    private static UserDao userDao;
    private static UserProfileDao userProfileDao;
    public static User user;
    public static User guest_user;
    public static UserProfile userProfile;
    public static UserProfile guest_user_profile;

    private static SharedPreferences sharedPreferences;
    private static String display_name;
    private static String display_user_token;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        Stetho.initializeWithDefaults(this);
        databaseHelper = new DatabaseHelper(this);
        sharedPreferences = getSharedPreferences("runsup", Context.MODE_PRIVATE);

        // Get DAOs for user and user profile
        try {
            userDao = new UserDao(App.databaseHelper.userDao());
            userProfileDao = new UserProfileDao(App.databaseHelper.userProfileDao());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Launched app");
        manageUsers();
    }

    private static void manageUsers() {
        display_user_token = sharedPreferences.getString("userId", null);
        display_name = sharedPreferences.getString("user_displayname", null);

        // For the first time
        if (display_name == null || display_user_token == null) {
            Log.d(TAG, "Running for first time, init guest field");
            display_name = "guest";
            display_user_token = "guest";

            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("user_displayname", "guest");
            editor.putString("userId", "guest");
            editor.apply();
        }

        // If user not logged in
        if (display_user_token.equals("guest") || display_name.equals("guest")) {
            Log.d(TAG, "Logging in as guest");
            createGuestUser();
        } else {
            // It should be logged in now
            try {
                // Get logged in user
                List<User> users = userDao.getUserByAuthToken(display_user_token);

                // Load logged user from database
                if (users.isEmpty()) {
                    Log.e(TAG, "createUser: No user credentials found");
                    return;
                } else {
                    user = users.get(0);
                }

                // Get all user profiles
                List<UserProfile> list = userProfileDao.getUserProfile(user);

                // If no user profile, create one
                if (list.isEmpty()) {
                    // Setting default data
                    userProfile = new UserProfile();
                    userProfile.setAge(22);
                    userProfile.setHeight(180);
                    userProfile.setWeight(60);
                    userProfile.setUser(user);
                    userProfileDao.createUserProfile(userProfile);
                } else {
                    userProfile = list.get(0);
                }

                if (userProfile == null) {
                    Log.e(TAG, "UserProfile is null");
                }
                if (user == null) {
                    Log.e(TAG, "User is null");
                }
                Log.d(TAG, "Logging in as logged in user " + user.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void createGuestUser() {
        try {
            ArrayList<User> guests = (ArrayList<User>)userDao.getUserByAuthToken("guest");
            Log.d(TAG, "Get all guest users, size: " + guests.size());
            if (guests.isEmpty()) {
                Log.d(TAG, "No guest user yet. Create it");
                guest_user = new User();
                guest_user.setAccId("guest");
                guest_user.setAuthToken("guest");
                userDao.createUser(guest_user);

                Log.d(TAG, "Create guest user profile");
                guest_user_profile = new UserProfile();
                guest_user_profile.setAge(22.0);
                guest_user_profile.setHeight(180.0);
                guest_user_profile.setWeight(60.0);
                guest_user_profile.setUser(guest_user);
                userProfileDao.createUserProfile(guest_user_profile);

                Log.d(TAG, "createGuestUser: first time: id is " + guest_user.getId());
                Log.d(TAG, "createGuestUserProfile: first time: id is " + guest_user_profile.getId());
            } else {
                guest_user = guests.get(0);
                guest_user_profile = userProfileDao.getUserProfile(guest_user.getId());
                Log.d(TAG, "createGuestUser: id is " + guest_user.getId() + " with profile " + guest_user_profile.getId());
            }
            user = guest_user;
            userProfile = guest_user_profile;
            Log.d(TAG, "Log in as guest user id: " + user.getId());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setGuestUser() {
        Log.d(TAG, "setGuestUser");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_displayname", "guest");
        editor.putString("userId", "guest");
        editor.putBoolean("userSignedIn", false);
        editor.apply();
        display_name = "guest";
        display_user_token = "guest";
        createGuestUser();
    }

    public static void addLoggedInuser(String AccId, FirebaseUser fuser) {
        try {
            ArrayList<User> tmp = (ArrayList<User>)userDao.getUserByAuthToken(fuser.getUid());
            // Najdemo prijavljenega uporabnika
            if (!tmp.isEmpty()) {
                user = tmp.get(0);
            } else {
                user = new User();
                user.setAccId(AccId);
                user.setAuthToken(fuser.getUid());
                userDao.createUser(user);
            }
            userProfile = userProfileDao.getUserProfile(user.getId());

            // Add user data to shared preferences
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("user_displayname", fuser.getDisplayName());
            editor.putString("userId", fuser.getUid());
            editor.putBoolean("userSignedIn", true);
            if (fuser.getPhotoUrl() != null) {
                editor.putString("user_photo", fuser.getPhotoUrl().toString());
                Log.d(TAG, fuser.getPhotoUrl().toString());
            }
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
