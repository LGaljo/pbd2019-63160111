package si.uni_lj.fri.pbd2019.runsup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncData;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.UserDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.UserProfileDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

public class LoginActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 3;
    private static final String TAG = LoginActivity.class.getSimpleName();

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    private SharedPreferences sharedPreferences;

    private UserDao userDao;
    private UserProfileDao userProfileDao;
    private GpsPointsDao gpsPointsDao;
    private WorkoutDao workoutDao;

    private String display_name;
    private boolean userSignedIn;

    @BindView(R.id.legal)
    TextView mStatusTextView;
    @BindView(R.id.sign_in_button)
    SignInButton signInButton;
    @BindView(R.id.weight_value)
    TextView weight_value;
    @BindView(R.id.age_value)
    TextView age_value;
    @BindView(R.id.height_value)
    TextView height_value;
    @BindView(R.id.login_status)
    TextView login_status;
    @BindView(R.id.sign_out_button)
    Button sign_out_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            userDao = new UserDao(App.databaseHelper.userDao());
            userProfileDao = new UserProfileDao(App.databaseHelper.userProfileDao());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showloginInfo();

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    private void showloginInfo() {
        sharedPreferences = getSharedPreferences("runsup", Context.MODE_PRIVATE);

        display_name = sharedPreferences.getString("user_displayname", null);
        userSignedIn = sharedPreferences.getBoolean("userSignedIn", false);

        if (!userSignedIn) {
            login_status.setText(getString(R.string.status_text_logged_out));
        } else {
            login_status.setText(getString(R.string.status_text_logged_in) + " as " + display_name);
            sign_out_button.setVisibility(View.VISIBLE);
        }
        if (App.userProfile != null) {
            weight_value.setText(String.format(getString(R.string.all_kilograms), App.userProfile.getWeight()));
            height_value.setText(String.format(getString(R.string.all_centimeters), App.userProfile.getHeight()));
            age_value.setText(String.format(getString(R.string.all_age), App.userProfile.getAge()));
        }
    }

    @OnClick(R.id.sign_in_button)
    public void sign_in() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.sign_out_button)
    public void sign_out() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        mGoogleSignInClient.signOut().addOnCompleteListener((task -> {
            sign_out_button.setVisibility(View.GONE);
            Snackbar.make(findViewById(R.id.login_layout), "Signed out.", Snackbar.LENGTH_SHORT).show();
            login_status.setText(getString(R.string.status_text_logged_out));
            Log.d(TAG, "User has logged out");
            // Delete user data
            App.setGuestUser();
            if (App.userProfile != null) {
                weight_value.setText(String.format(getString(R.string.all_kilograms), App.userProfile.getWeight()));
                height_value.setText(String.format(getString(R.string.all_centimeters), App.userProfile.getHeight()));
                age_value.setText(String.format(getString(R.string.all_age), App.userProfile.getAge()));
            }
        }));
    }

    @OnClick(R.id.delete_firebase_data)
    public void deleteFirebaseData() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_delete_remote_data)
                .setPositiveButton(R.string.yes, (DialogInterface dialog, int which) -> {
                    if (SyncData.deleteFirebaseData()) {
                        Snackbar.make(findViewById(android.R.id.content), "Uploaded data deleted.", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "Error.", Snackbar.LENGTH_LONG)
                                .setAction("No action", null).show();
                    }
                })
                .setNegativeButton(R.string.no, (DialogInterface dialog, int which) -> {
                    // Close dialog and do nothing
                    dialog.dismiss();
                })
                .show();
    }

    @OnClick(R.id.delete_local_data)
    public void deleteLocalData() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.dialog_delete_local_data_message)
                .setPositiveButton(R.string.yes, (DialogInterface dialog, int which) -> {
                    gpsPointsDao.deleteAll();
                    ArrayList<Workout> list = (ArrayList<Workout>) workoutDao.getAllWorkouts(true);
                    for (Workout w : list) {
                        w.setStatus(Workout.statusDeleted);
                        workoutDao.updateWorkout(w);
                    }
                    sign_out();
                })
                .setNegativeButton(R.string.no, (DialogInterface dialog, int which) -> {
                    // Close dialog and do nothing
                    dialog.dismiss();
                })
                .show();
    }

    @SuppressLint("DefaultLocale")
    @OnClick(R.id.weight_layout_container)
    public void setWeight() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Get the layout inflater and set layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_data, null);

        // Get EditText field
        EditText edit_title = dialogView.findViewById(R.id.edit_field);
        try {
            edit_title.setText(String.format("%.1f", App.userProfile.getWeight()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set view
        builder.setView(dialogView);

        // Set title
        builder.setTitle("Change weight");

        // Add the buttons
        builder.setPositiveButton(R.string.ok, (DialogInterface dialog, int id) -> {
            // User clicked OK button
            dialog.cancel();
            try {
                double weight = Double.parseDouble(edit_title.getText().toString());
                App.userProfile.setWeight(weight);
                userProfileDao.updateUserProfile(App.userProfile);
                weight_value.setText(String.format(getString(R.string.all_kilograms), weight));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        builder.setNegativeButton(R.string.cancel, (DialogInterface dialog, int id) -> {
            // User clicked Cancel button
            dialog.dismiss();
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint("DefaultLocale")
    @OnClick(R.id.age_layout_container)
    public void setAge() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Get the layout inflater and set layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_data, null);

        // Get EditText field
        EditText edit_title = dialogView.findViewById(R.id.edit_field);
        try {
            edit_title.setText(String.format("%.0f", App.userProfile.getAge()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set view
        builder.setView(dialogView);

        // Set title
        builder.setTitle("Change age");

        // Add the buttons
        builder.setPositiveButton(R.string.ok, (DialogInterface dialog, int id) -> {
            // User clicked OK button
            dialog.cancel();
            try {
                double age = Double.parseDouble(edit_title.getText().toString());
                App.userProfile.setAge(age);
                userProfileDao.updateUserProfile(App.userProfile);
                age_value.setText(String.format(getString(R.string.all_age), age));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        builder.setNegativeButton(R.string.cancel, (DialogInterface dialog, int id) -> {
            // User clicked Cancel button
            dialog.dismiss();
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint("DefaultLocale")
    @OnClick(R.id.height_layout_container)
    public void setHeight() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Get the layout inflater and set layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_edit_data, null);

        // Get EditText field
        EditText edit_title = dialogView.findViewById(R.id.edit_field);
        try {
            edit_title.setText(String.format("%.1f", App.userProfile.getHeight()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set view
        builder.setView(dialogView);

        // Set title
        builder.setTitle("Change height");

        // Add the buttons
        builder.setPositiveButton(R.string.ok, (DialogInterface dialog, int id) -> {
            // User clicked OK button
            dialog.cancel();
            try {
                double height = Double.parseDouble(edit_title.getText().toString());
                App.userProfile.setHeight(height);
                userProfileDao.updateUserProfile(App.userProfile);
                height_value.setText(String.format(getString(R.string.all_centimeters), height));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // User clicked Cancel button
        builder.setNegativeButton(R.string.cancel, (DialogInterface dialog, int id) -> {
            dialog.dismiss();
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser fuser = mAuth.getCurrentUser();
                            try {
                                if (fuser != null) {
                                    Log.d(TAG, fuser.getDisplayName());

                                    // Add data to object in App and update
                                    App.addLoggedInuser(acct.getIdToken(), fuser);
                                    if (App.userProfile != null) {
                                        weight_value.setText(String.format(getString(R.string.all_kilograms), App.userProfile.getWeight()));
                                        height_value.setText(String.format(getString(R.string.all_centimeters), App.userProfile.getHeight()));
                                        age_value.setText(String.format(getString(R.string.all_age), App.userProfile.getAge()));
                                    }
                                }
                                login_status.setText(getString(R.string.status_text_logged_in) + " as " + display_name);
                                sign_out_button.setVisibility(View.VISIBLE);
                                Snackbar.make(findViewById(R.id.login_layout), "Authentication Successful.", Snackbar.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.login_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
