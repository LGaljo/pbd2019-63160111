package si.uni_lj.fri.pbd2019.runsup.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity;
import si.uni_lj.fri.pbd2019.runsup.App;
import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.SettingsActivity;
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity;
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper;
import si.uni_lj.fri.pbd2019.runsup.helpers.SyncData;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService;

import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.CYCLING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.RUNNING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.UNDEFINED;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.WALKING;
import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.STATE_CONTINUE;
import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.STATE_PAUSED;
import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.STATE_STOPPED;
import static si.uni_lj.fri.pbd2019.runsup.services.TrackerService.UPDATE_SPORT_ACTIVITY;

public class StopwatchFragment extends Fragment {
    private static final String TAG = StopwatchFragment.class.getSimpleName();

    @BindView(R.id.textview_stopwatch_duration)
    TextView textview_stopwatch_duration;
    @BindView(R.id.textview_stopwatch_distance)
    TextView textview_stopwatch_distance;
    @BindView(R.id.textview_stopwatch_pace)
    TextView textview_stopwatch_pace;
    @BindView(R.id.textview_stopwatch_calories)
    TextView textview_stopwatch_calories;
    @BindView(R.id.textview_stopwatch_altitude)
    TextView textview_stopwatch_altitude;
    @BindView(R.id.textview_stopwatch_speed)
    TextView textview_stopwatch_speed;

    @BindView(R.id.button_stopwatch_selectsport)
    Button button_stopwatch_selectsport;
    @BindView(R.id.button_stopwatch_start)
    Button button_stopwatch_start;
    @BindView(R.id.button_stopwatch_endworkout)
    Button button_stopwatch_endworkout;
    @BindView(R.id.button_stopwatch_activeworkout)
    Button button_stopwatch_activeworkout;

    private BroadcastReceiver mBroadcastReceiver;

    private SharedPreferences sharedPref;
    private SharedPreferences settings;

    private Long workoutId = -1L;
    private static int type = WALKING;
    private static int state = STATE_STOPPED;

    private boolean userSignedIn;

    private String units_dist;
    private String units_pace;
    private String units_speed;
    private String units_altitude;

    private boolean preference_gps;
    private int preference_units;

    private boolean hasRespondedToLocationRequest = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        sharedPref = getActivity().getSharedPreferences("runsup", Context.MODE_PRIVATE);
        userSignedIn = sharedPref.getBoolean("userSignedIn", false);

        manageUnits();
        manageBroadcastReceiver();
        checkForLocationPerms();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        ButterKnife.bind(this, view);
        setOnClickListeners();

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preference_gps = settings.getBoolean(SettingsActivity.GPS, true);
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);

        restoreWorkout();

        Intent intent = new Intent(getActivity(), TrackerService.class);
        getActivity().startForegroundService(intent);
        return view;
    }

    @SuppressLint("SetTextI18n")
    private boolean restoreWorkout() {
        WorkoutDao workoutDao = null;
        try {
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
            List<Workout> w = workoutDao.getAllWorkoutsByUser(true, App.user);
            if (!w.isEmpty() && (w.get(w.size() - 1).getStatus() == Workout.statusPaused || w.get(w.size() - 1).getStatus() == Workout.statusUnknown)) {
                Workout workout = w.get(w.size() - 1);

                // TODO get alt and speed
                textview_stopwatch_duration.setText(MainHelper.formatDuration(workout.getDuration()));
                if (preference_units == 1) {
                    textview_stopwatch_distance.setText(MainHelper.formatDistance(MainHelper.kmToMi(workout.getDistance())) + " " + units_dist);
                    textview_stopwatch_pace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(workout.getPaceAvg())) + " " + units_pace);
                    textview_stopwatch_altitude.setText(MainHelper.formatAltitude(0) + " " + units_altitude);
                    textview_stopwatch_speed.setText(MainHelper.formatSpeed(MainHelper.mpsToMiph(0)) + " " + units_speed);
                } else {
                    textview_stopwatch_distance.setText(MainHelper.formatDistance(workout.getDistance()) + " " + units_dist);
                    textview_stopwatch_pace.setText(MainHelper.formatPace(workout.getPaceAvg()) + " " + units_pace);
                    textview_stopwatch_altitude.setText(MainHelper.formatAltitude(0) + " " + units_altitude);
                    textview_stopwatch_speed.setText(MainHelper.formatSpeed(MainHelper.mpsToKmph(0)) + " " + units_speed);
                }

                textview_stopwatch_calories.setText(MainHelper.formatCalories(workout.getTotalCalories()));

                switch (workout.getSportActivity()) {
                    case RUNNING:
                        button_stopwatch_selectsport.setText(getResources().getText(R.string.running).toString());
                        break;
                    case WALKING:
                        button_stopwatch_selectsport.setText(getResources().getText(R.string.walking).toString());
                        break;
                    case CYCLING:
                        button_stopwatch_selectsport.setText(getResources().getText(R.string.cycling).toString());
                        break;
                }
                button_stopwatch_selectsport.setEnabled(true);

                type = workout.getSportActivity();
                workoutId = workout.getId();

                button_stopwatch_start.setText(getResources().getString(R.string.stopwatch_continue));
                button_stopwatch_endworkout.setText(getResources().getString(R.string.stopwatch_endworkout));
                button_stopwatch_endworkout.setVisibility(View.VISIBLE);

                button_stopwatch_start.setOnClickListener((View v) -> startWorkout());

                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.clear();
        menu.add(0, 0, Menu.NONE, getString(R.string.action_settings))
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        if (userSignedIn) {
            menu.add(0, 1, Menu.NONE, getString(R.string.historysettings_actionsync))
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 1) {
            Thread thread = new Thread(() -> {
                getActivity().runOnUiThread(() -> {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Syncing in background", Snackbar.LENGTH_LONG).setAction("No action", null).show();
                });
                SyncData.sync();
            });
            thread.start();
        } else if (item.getItemId() == 0) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop Tracker Service if no workout is running / Do nothing if workout in progress
        if (state == STATE_STOPPED) {
            Intent intent = new Intent(getActivity(), TrackerService.class);
            getActivity().stopService(intent);
        }
        try {
            getActivity().unregisterReceiver(mBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        preference_gps = settings.getBoolean(SettingsActivity.GPS, true);

        manageUnits();

        // Start Tracker Service if no workout is running / Do nothing if workout in progress
        if (state == STATE_STOPPED) {
            Intent intent = new Intent(getActivity(), TrackerService.class);
            getActivity().startService(intent);
        }
        try {
            getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(TrackerService.TICK));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Intent intent = new Intent(getActivity(), TrackerService.class);
            getActivity().stopService(intent);
            getActivity().unregisterReceiver(mBroadcastReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void checkForLocationPerms() {
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // TODO pls dont deny
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                hasRespondedToLocationRequest = false;

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            }
        } else {
            // Permission has already been granted
            Log.d(TAG, "checkForLocationPerms: Location permissions granted");
            hasRespondedToLocationRequest = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Got permissions
                Toast.makeText(getActivity(), "Location permissions granted", Toast.LENGTH_SHORT).show();
                hasRespondedToLocationRequest = true;
            }
        }
    }

    private void manageBroadcastReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onReceive(Context context, Intent intent) {
                long duration;
                double distance;
                double calories;
                Double pace;
                Float speed;
                double altitude;

                if (intent.getAction() != null && intent.getAction().equals(TrackerService.TICK)) {
                    workoutId = intent.getLongExtra("workoutId", 0);
                    duration = intent.getLongExtra("duration", -1);
                    distance = intent.getDoubleExtra("distance", -1);
                    pace = intent.getDoubleExtra("pace", -1);
                    calories = intent.getDoubleExtra("calories", -1);
                    speed = intent.getFloatExtra("speed", -1);
                    altitude = intent.getDoubleExtra("altitude", -1);
                    state = intent.getIntExtra("state", -1);
                    type = intent.getIntExtra("sportActivity", WALKING);

                    switch (state) {
                        case STATE_PAUSED:
                            pauseWorkout(false);
                            break;
                        case STATE_CONTINUE:
                            continueWorkout(false);
                            break;
                    }

                    switch (type) {
                        case RUNNING:
                            button_stopwatch_selectsport.setText(getResources().getText(R.string.running).toString());
                            break;
                        case WALKING:
                            button_stopwatch_selectsport.setText(getResources().getText(R.string.walking).toString());
                            break;
                        case CYCLING:
                            button_stopwatch_selectsport.setText(getResources().getText(R.string.cycling).toString());
                            break;
                    }

                    if (speed.isNaN()) {
                        speed = 0f;
                    }
                    if (pace.isNaN()) {
                        pace = 0.0;
                    }

                    textview_stopwatch_duration.setText(MainHelper.formatDuration(duration));
                    if (preference_units == 1) {
                        textview_stopwatch_distance.setText(MainHelper.formatDistance(MainHelper.kmToMi(distance)) + " " + units_dist);
                        textview_stopwatch_pace.setText(MainHelper.formatPace(MainHelper.minpkmToMinpmi(pace)) + " " + units_pace);
                        textview_stopwatch_altitude.setText(MainHelper.formatAltitude(MainHelper.metersToFeet(altitude)) + " " + units_altitude);
                        textview_stopwatch_speed.setText(MainHelper.formatSpeed(MainHelper.mpsToKmph(speed)) + " " + units_speed);
                    } else {
                        textview_stopwatch_distance.setText(MainHelper.formatDistance(distance) + " " + units_dist);
                        textview_stopwatch_pace.setText(MainHelper.formatPace(pace) + " " + units_pace);
                        textview_stopwatch_altitude.setText(MainHelper.formatAltitude(altitude) + " " + units_altitude);
                        textview_stopwatch_speed.setText(MainHelper.formatSpeed(MainHelper.mpsToKmph(speed)) + " " + units_speed);
                    }
                    textview_stopwatch_calories.setText(MainHelper.formatCalories(calories));
                }
            }
        };

        getActivity().registerReceiver(mBroadcastReceiver, new IntentFilter(TrackerService.TICK));
    }

    private void setOnClickListeners() {
        button_stopwatch_start.setOnClickListener((View v) -> {
            if (type == UNDEFINED) {
                type = WALKING;
                button_stopwatch_selectsport.setText(getResources().getText(R.string.walking).toString());
            }
            button_stopwatch_selectsport.setEnabled(false);
            startWorkout();
        });

        button_stopwatch_endworkout.setOnClickListener((View v) -> {
            showDialogEndActivity();
        });
    }

    @OnClick(R.id.button_stopwatch_selectsport)
    public void selectSport() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater and set layout
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_select_sport, null);

        // Set view
        builder.setView(dialogView);

        //  Set message
        builder.setMessage(getString(R.string.select_sport));

        // Add the buttons
        builder.setPositiveButton(R.string.ok, (DialogInterface dialog, int id) -> {
            RadioGroup radioGroup = dialogView.findViewById(R.id.radiogroup_select_sport);
            int selectedSport = UNDEFINED;

            switch (radioGroup.getCheckedRadioButtonId()) {
                case R.id.walking:
                    selectedSport = WALKING;
                    break;
                case R.id.running:
                    selectedSport = RUNNING;
                    break;
                case R.id.cycling:
                    selectedSport = CYCLING;
                    break;
            }

            switch (selectedSport) {
                case RUNNING:
                    button_stopwatch_selectsport.setText(getResources().getText(R.string.running).toString());
                    break;
                case WALKING:
                    button_stopwatch_selectsport.setText(getResources().getText(R.string.walking).toString());
                    break;
                case CYCLING:
                    button_stopwatch_selectsport.setText(getResources().getText(R.string.cycling).toString());
                    break;
            }

            // Send selected sport
            type = selectedSport;
            Intent intent = new Intent(getActivity(), TrackerService.class);
            intent.putExtra("sportActivity", type);
            intent.setAction(UPDATE_SPORT_ACTIVITY);
            getActivity().startService(intent);
        });

        builder.setNegativeButton(R.string.cancel, (DialogInterface dialog, int id) -> {
            // User clicked Cancel button
            dialog.dismiss();
        });

        // Create the AlertDialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @OnClick(R.id.button_stopwatch_activeworkout)
    public void showActiveWorkoutMap() {
        Intent intent = new Intent(getActivity(), ActiveWorkoutMapActivity.class);
        Log.d(TAG, "showActiveWorkoutMap of workout " + workoutId);
        if (workoutId != -1L) {
            intent.putExtra("workoutId", workoutId);
        }
        startActivity(intent);
    }

    private void manageUnits() {
        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String pref_units = settings.getString(SettingsActivity.UNITS, "0");
        preference_units = Integer.parseInt(pref_units);
        units_dist = getContext().getString(R.string.all_labeldistanceunitkilometers);
        units_pace =
                getContext().getString(R.string.all_min) + "/"
                        + getContext().getString(R.string.all_labeldistanceunitkilometers);
        units_altitude = getContext().getString(R.string.all_labelaltitudeunitmeters);
        units_speed = getContext().getString(R.string.all_labeldistanceunitkilometers) + "/"
                + getContext().getString(R.string.all_labelhour);
        if (preference_units == 1) {
            units_dist = getContext().getString(R.string.all_labeldistanceunitmiles);
            units_pace = getContext().getString(R.string.all_min) + "/"
                    + getContext().getString(R.string.all_labeldistanceunitmiles);
            units_altitude = getContext().getString(R.string.all_labelaltitudeunitfeets);
            units_speed = getContext().getString(R.string.all_labeldistanceunitmiles) + "/"
                    + getContext().getString(R.string.all_labelhour);
        }
    }

    private void pauseWorkout(boolean sendAction) {
        button_stopwatch_start.setText(getResources().getString(R.string.stopwatch_continue));
        button_stopwatch_endworkout.setText(getResources().getString(R.string.stopwatch_endworkout));
        button_stopwatch_endworkout.setVisibility(View.VISIBLE);
        if (sendAction) {
            Intent intent = new Intent(getActivity(), TrackerService.class);
            intent.setAction(TrackerService.COMMAND_PAUSE);
            getActivity().startService(intent);
        }

        button_stopwatch_start.setOnClickListener((View v) -> continueWorkout(true));
    }

    private void continueWorkout(boolean sendAction) {
        if (preference_gps) {
            checkForLocationPerms();
            if (!hasRespondedToLocationRequest) {
                Snackbar.make(
                        getActivity().findViewById(android.R.id.content),
                        "Denied location permission. Change that in app info.",
                        Snackbar.LENGTH_LONG)
                        .setAction("App info", view -> {
                                    Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    i.addCategory(Intent.CATEGORY_DEFAULT);
                                    i.setData(Uri.parse("package:" + getActivity().getPackageName()));
                                    startActivity(i);
                                }
                        ).show();
            } else {
                button_stopwatch_start.setText(getResources().getString(R.string.stopwatch_stop));
                button_stopwatch_endworkout.setVisibility(View.GONE);
                if (sendAction) {
                    Intent intent = new Intent(getActivity(), TrackerService.class);
                    intent.setAction(TrackerService.COMMAND_CONTINUE);
                    getActivity().startService(intent);
                }

                button_stopwatch_start.setOnClickListener((View v) -> pauseWorkout(true));
            }
        } else {
            Snackbar.make(
                    getActivity().findViewById(android.R.id.content),
                    "Request for location is disabled. Change that in settings.",
                    Snackbar.LENGTH_LONG)
                    .setAction("Settings", view -> startActivity(new Intent(getActivity(), SettingsActivity.class))
                    ).show();
        }
    }

    private void startWorkout() {
        if (preference_gps) {
            checkForLocationPerms();
            if (!hasRespondedToLocationRequest) {
                Snackbar.make(
                        getActivity().findViewById(android.R.id.content),
                        "Denied location permission. Change that in app info.",
                        Snackbar.LENGTH_LONG)
                        .setAction("App info", view -> {
                                    Intent i = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    i.addCategory(Intent.CATEGORY_DEFAULT);
                                    i.setData(Uri.parse("package:" + getActivity().getPackageName()));
                                    startActivity(i);
                                }
                        ).show();
            } else {
                button_stopwatch_start.setText(getResources().getString(R.string.stopwatch_stop));
                button_stopwatch_endworkout.setVisibility(View.GONE);
                Intent intent = new Intent(getActivity(), TrackerService.class);
                intent.setAction(TrackerService.COMMAND_START);
                intent.putExtra("sportActivity", type);
                getActivity().startService(intent);

                button_stopwatch_start.setOnClickListener((View v) -> pauseWorkout(true));
            }
        } else {
            Snackbar.make(
                    getActivity().findViewById(android.R.id.content),
                    "Request for location is disabled. Change that in settings.",
                    Snackbar.LENGTH_LONG)
                    .setAction("Settings", view -> startActivity(new Intent(getActivity(), SettingsActivity.class))
                    ).show();
        }
    }

    private void endWorkout() {
        Intent intent = new Intent(getActivity(), TrackerService.class);
        intent.setAction(TrackerService.COMMAND_STOP);
        getActivity().startService(intent);
    }

    @SuppressLint("SetTextI18n")
    private void resetActivity() {
        textview_stopwatch_duration.setText(MainHelper.formatDuration(0));
        textview_stopwatch_distance.setText(MainHelper.formatDistance(0) + " " + units_dist);
        textview_stopwatch_pace.setText(MainHelper.formatPace(0) + " " + units_pace);
        textview_stopwatch_calories.setText(MainHelper.formatCalories(0));
        textview_stopwatch_speed.setText(MainHelper.formatSpeed(0) + " " + units_speed);
        textview_stopwatch_altitude.setText(MainHelper.formatAltitude(0) + " " + units_altitude);

        button_stopwatch_selectsport.setText(getResources().getText(R.string.select_sport).toString());
        button_stopwatch_selectsport.setEnabled(true);
        button_stopwatch_start.setText(getString(R.string.stopwatch_start));
        button_stopwatch_endworkout.setVisibility(View.GONE);
        type = UNDEFINED;
        workoutId = -1L;
        setOnClickListeners();
    }

    private void showDialogEndActivity() {

        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.dialog_message)
                .setPositiveButton(R.string.yes, (DialogInterface dialog, int which) -> {
                    //getActivity().stopService(new Intent(getActivity(), TrackerService.class));
                    // End workout
                    endWorkout();

                    // Append data
                    Intent intent = new Intent(getActivity(), WorkoutDetailActivity.class);
                    intent.putExtra("workoutId", workoutId);
                    resetActivity();

                    // Reset activity view
                    startActivity(intent);
                })
                .setNegativeButton(R.string.no, (DialogInterface dialog, int which) -> {
                    // Close dialog and do nothing
                    button_stopwatch_endworkout.setText(R.string.stopwatch_endworkout);
                    dialog.dismiss();
                })
                .show();
    }
}
