package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Workout")
public class Workout {
    // initial status of workout
    public static final int statusUnknown = 0;
    // ended workout
    public static final int statusEnded = 1;
    // paused workout
    public static final int statusPaused = 2;
    // deleted workout
    public static final int statusDeleted = 3;

    @DatabaseField(generatedId = true, canBeNull = false, unique = true)
    private Long id;
    @DatabaseField(canBeNull = false, foreign = true)
    private User user;
    @DatabaseField()
    private String title;
    @DatabaseField(version = true, format = "yyyy-MM-dd HH:mm:ss", dataType = DataType.DATE, columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL", readOnly = true, canBeNull = false)
    private Date created;
    @DatabaseField(version = true, format = "yyyy-MM-dd HH:mm:ss", dataType = DataType.DATE)
    private Date lastUpdated;
    @DatabaseField()
    private int status;
    @DatabaseField()
    private long duration;
    @DatabaseField()
    private double distance;
    @DatabaseField()
    private double totalCalories;
    @DatabaseField()
    private double paceAvg;
    @DatabaseField()
    private int sportActivity;

    public Workout() {
    }

    public Workout(String title, int sportActivity) {
        this.title = title;
        this.sportActivity = sportActivity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public double getPaceAvg() {
        return paceAvg;
    }

    public void setPaceAvg(double paceAvg) {
        this.paceAvg = paceAvg;
    }

    public int getSportActivity() {
        return sportActivity;
    }

    public void setSportActivity(int sportActivity) {
        this.sportActivity = sportActivity;
    }

    @Override
    public String toString() {
        return this.id + ": " + this.duration + " " + this.distance;
    }
}
