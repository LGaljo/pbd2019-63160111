package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.App;
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;
import si.uni_lj.fri.pbd2019.runsup.model.Workout;
import si.uni_lj.fri.pbd2019.runsup.model.daos.GpsPointsDao;
import si.uni_lj.fri.pbd2019.runsup.model.daos.WorkoutDao;

public class SyncData {
    private static final String TAG = HistoryFragment.class.getSimpleName();

    private static final String WORKOUT_COLUMN_NAME = "workouts";
    private static final String USERS_COLUMN_NAME = "users";
    private static final String GPSPOINTS_COLUMN_NAME = "gps_points";

    private static DatabaseReference mDatabase;
    private static String mUserId;

    private static GpsPointsDao gpsPointsDao;
    private static WorkoutDao workoutDao;

    private static ArrayList<Workout> workouts = new ArrayList<>();
    private static ArrayList<Workout> tmpWorkouts = new ArrayList<>();

    private static boolean init() {
        // Initialize Firebase Auth and Database Reference
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
            Log.d(TAG, "loadData: Cant find Firebase user");
            return false;
        }

        mUserId = mFirebaseUser.getUid();
        Log.d(TAG, "User id: " + mFirebaseUser.getUid());

        try {
            gpsPointsDao = new GpsPointsDao(App.databaseHelper.gpsPointDao());
            workoutDao = new WorkoutDao(App.databaseHelper.workoutDao());
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static ArrayList<Workout> sync() {
        // Pridobivanje lokalnih in oddaljenih podatkov
        if (!loadData()) {
            return null;
        }

        // Lokalne podatke poslji na Firebase
        for (Workout workout : workouts) {
            if (workout != null) {
                // Firebase ne dovoli Inf vrednosti
                if (workout.getPaceAvg() == Double.POSITIVE_INFINITY) {
                    workout.setPaceAvg(-1.0);
                }

                // Poslji
                mDatabase.child(USERS_COLUMN_NAME).child(mUserId).child(WORKOUT_COLUMN_NAME).child(workout.getCreated().toString()).setValue(workout);
                // Dodaj se gps podatke tega workouta
                ArrayList<GpsPoint> gpsPointList = new ArrayList<>(gpsPointsDao.getGpsPointsByWorkout(workout));
                for (GpsPoint g : gpsPointList) {
                    // Firebase ne dovoli Inf vrednosti
                    if (g.getPace() == Double.POSITIVE_INFINITY) {
                        g.setPace(-1.0);
                    }
                    // Poslji
                    mDatabase.child(USERS_COLUMN_NAME).child(mUserId).child(GPSPOINTS_COLUMN_NAME).child(g.getId().toString()).setValue(g);
                }
            }
        }

        return tmpWorkouts;
    }

    private static boolean loadData() {
        // Inicializacija vmesnikov
        if (!init()) {
            return false;
        }

        // Pridobi lokalne podatke
        workouts = (ArrayList<Workout>) workoutDao.getAllWorkoutsByUser(true, App.user);
        if (workouts.isEmpty()) {
            Log.d(TAG, "sync: No workouts locally found");
        }

        mDatabase.child(USERS_COLUMN_NAME).child(mUserId).child(GPSPOINTS_COLUMN_NAME).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                GpsPoint g = dataSnapshot.getValue(GpsPoint.class);
                if (g != null) {
                    gpsPointsDao.createOrUpdateGpsPoint(g);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDatabase.child(USERS_COLUMN_NAME).child(mUserId).child(WORKOUT_COLUMN_NAME).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                // Pridobi oddaljene podatke
                Workout q = dataSnapshot.getValue(Workout.class);
                if (q != null) {
                    if (q.getStatus() == 3) {
                        q.setStatus(1);
                    }
                    workoutDao.createOrUpdateWorkout(q);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return true;
    }

    public static boolean deleteFirebaseData() {
        if (!init()) {
            return false;
        }
        mDatabase.child(USERS_COLUMN_NAME).child(mUserId).removeValue();
        return true;
    }
}
