package si.uni_lj.fri.pbd2019.runsup.model.daos;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import si.uni_lj.fri.pbd2019.runsup.model.User;

public class UserDao {
    private Dao<User, Long> userDao;

    public UserDao(Dao<User, Long> userDao) {
        this.userDao = userDao;
    }

    public User getUser(Long id) {
        try {
            return userDao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getUserByAuthToken(String token) {
        try {
            return userDao.queryBuilder()
                    .where()
                    .eq("authToken", token)
                    .query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public List<User> getAllUsers() {
        try {
            return userDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteUser(Long id) {
        try {
            userDao.delete(getUser(id));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(User user) {
        try {
            userDao.delete(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createOrUpdateUser(User user) {
        try {
            userDao.createOrUpdate(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createUser(User user) {
        try {
            userDao.create(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateUser(User user) {
        try {
            userDao.update(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            DeleteBuilder<User, Long> builder =  userDao.deleteBuilder();
            builder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
