package si.uni_lj.fri.pbd2019.runsup.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.runsup.R;
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint;

import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.CYCLING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.RUNNING;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.UNDEFINED;
import static si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities.WALKING;

final public class MainHelper {
    private static final String TAG = MainHelper.class.getSimpleName();

    /* constants */
    private static final float MpS_TO_MIpH = 2.23694f;
    private static final float KM_TO_MI = 0.62137119223734f;
    private static final float MINpKM_TO_MINpMI = 1.609344f;
    private static final float METERS_TO_FEET = 3.2808399f;
    private static final float MI_TO_FEET = 5280;
    private static final float FEET_TO_MI = 0.000189393939f;
    private static final float MpS_TO_KMpH = 3.6f;

    /**
     * return string of time in format HH:MM:SS
     *
     * @param duration - in seconds
     */
    @SuppressLint("DefaultLocale")
    public static String formatDuration(long duration) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%01d", duration / 3600));
        sb.append(":");
        sb.append(String.format("%02d", (duration % 3600) / 60));
        sb.append(":");
        sb.append(String.format("%02d", (duration % 3600) % 60));
        return sb.toString();
    }

    public static String formatActivityType(Context context, int type) {
        switch (type) {
            case RUNNING:
                return context.getResources().getText(R.string.running).toString();
            case WALKING:
                return context.getResources().getText(R.string.walking).toString();
            case CYCLING:
                return context.getResources().getText(R.string.cycling).toString();
            case UNDEFINED:
                return context.getResources().getText(R.string.unknown).toString();
        }
        return "";
    }

    /**
     * convert m to km and round to 2 decimal places and return as string
     */
    @SuppressLint("DefaultLocale")
    public static String formatDistance(double n) {
        return String.format("%.2f", n / 1000);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    @SuppressLint("DefaultLocale")
    public static String formatPace(double n) {
        return String.format("%.2f", n);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    @SuppressLint("DefaultLocale")
    public static String formatSpeed(double n) {
        return String.format("%.2f", n);
    }

    /**
     * round number to 2 decimal places and return as string
     */
    @SuppressLint("DefaultLocale")
    public static String formatAltitude(double n) {
        return String.format("%.0f", n);
    }

    /**
     * round number to integer
     */
    @SuppressLint("DefaultLocale")
    public static String formatCalories(double n) {
        return String.format("%d kcal", Math.round(n));
    }

    /**
     * Funkcija za pretvorbo "pixel" v "density independent pixel"
     *
     * @param px - vrednost piklsov
     * @return - vrednost density independent pixel
     */
    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * Funkcija za pretvorbo "density independent pixel" v "pixel"
     *
     * @param dp - vrednost density independent pixel
     * @return - vrednost piklsov
     */
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * Create PolyLine object
     */
    public static Polyline createPolyLine(ArrayList<GpsPoint> gpsPoints, GoogleMap googleMap, boolean show_last) {
        long session = 0L;
        int ind = 0;

        PolylineOptions polylineOptions = new PolylineOptions().width(5).color(Color.BLUE);

        if (gpsPoints != null && !gpsPoints.isEmpty()) {
            LatLng prev = null;

            for (GpsPoint g : gpsPoints) {
                LatLng latLng = new LatLng(g.getLatitude(), g.getLongitude());

                if (session != g.getSessionNumber()) {
                    session = g.getSessionNumber();
                    addRedCircle(googleMap, latLng);
                    addRedCircle(googleMap, prev);
                    googleMap.addPolyline(polylineOptions);
                    polylineOptions = new PolylineOptions().width(5).color(Color.BLUE);
                }
                if (ind == 0 || (ind == gpsPoints.size() - 1 && show_last)) {
                    addBlueCircle(googleMap, latLng);
                }
                polylineOptions.add(latLng);
                ind += 1;
                prev = new LatLng(g.getLatitude(), g.getLongitude());
            }
        }
        return googleMap.addPolyline(polylineOptions);
    }

    /**
     * Create PolyLine object
     */
    public static void createPolyLineWithLabels(Context context, ArrayList<GpsPoint> gpsPoints, GoogleMap gMa) {
        long session = 0L;
        int ind = 0;

        IconGenerator tc = new IconGenerator(context);
        PolylineOptions polylineOptions = new PolylineOptions().width(5).color(Color.BLUE);

        if (gpsPoints != null && !gpsPoints.isEmpty()) {
            LatLng prev = null;
            LatLng first = new LatLng((float)gpsPoints.get(0).getLatitude(), (float)gpsPoints.get(0).getLongitude());
            LatLng last = new LatLng((float)gpsPoints.get(gpsPoints.size() - 1).getLatitude(), (float)gpsPoints.get(gpsPoints.size() - 1).getLongitude());

            addMarker(gMa, "Start", first, tc);
            addMarker(gMa, "Finish", last, tc);

            for (GpsPoint g : gpsPoints) {
                LatLng latLng = new LatLng(g.getLatitude(), g.getLongitude());

                if (session != g.getSessionNumber()) {
                    session = g.getSessionNumber();
                    if (distance(prev, latLng) > 100) {
                        addMarker(gMa, "Continue " + session, latLng, tc);
                        addMarker(gMa, "Pause " + session, prev, tc);
                        addRedCircle(gMa, latLng);
                        addRedCircle(gMa, prev);
                        gMa.addPolyline(polylineOptions);
                        polylineOptions = new PolylineOptions().width(5).color(Color.BLUE);
                    } else {
                        addRedCircle(gMa, latLng);
                        addRedCircle(gMa, prev);
                        addMarker(gMa, "Break " + session, prev, tc);
                    }
                }
                if (gpsPoints.size() - 1 == ind || ind == 0) {
                    addBlueCircle(gMa, latLng);
                }
                polylineOptions.add(latLng);
                ind += 1;
                prev = new LatLng(g.getLatitude(), g.getLongitude());
            }
            gMa.addPolyline(polylineOptions);
        }
    }

    private static double distance(LatLng first, LatLng second) {
        return SphericalUtil.computeDistanceBetween(first, second);
    }

    private static void addMarker(GoogleMap gMa, String label, LatLng latLng, IconGenerator tc) {
        gMa.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(tc.makeIcon(label))))
                .showInfoWindow();

    }

    private static void addBlueCircle(GoogleMap googleMap, LatLng latLng) {
        googleMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(1)
                .strokeColor(Color.BLUE)
                .fillColor(0x220000FF));
    }

    private static void addRedCircle(GoogleMap googleMap, LatLng latLng) {
        if (latLng != null) {
            googleMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(1)
                    .strokeColor(Color.RED)
                    .fillColor(0x22FF0000));
        }
    }

    /* convert km to mi */
    public static double kmToMi(double n) {
        return n * KM_TO_MI;
    }

    /* convert m/s to mi/h */
    public static double mpsToMiph(double n) {
        return n * MpS_TO_MIpH;
    }

    /* convert min/km to min/mi */
    public static double minpkmToMinpmi(double n) {
        return n * MINpKM_TO_MINpMI;
    }

    public static double metersToFeet(double n) {
        return n * METERS_TO_FEET;
    }

    public static double mpsToKmph(double n) {
        return n * MpS_TO_KMpH;
    }

    public static double mileToFeet(double n) {
        return n * MI_TO_FEET;
    }

    public static double feetToMile(double n) {
        return n * FEET_TO_MI;
    }
}
