# RunsUp #

An application created for a course Programiranje specifičnih platform.

### General author info ###

* lg0775@student.uni-lj.si
* Luka Galjot

### Description ###

This app allow tracking of physical activities and storing them.

### Additional features in Sprint 4 ###
Track your workout all the time. Service has been converted to foreground service. So even when you exit your app service will continue to function as expected. Before that location callbacks would stop and workout would be very boring with only time you had.

Notifications were added to allow you to view your progress even when you are using other apps. There are duration and distance info, which are units aware from settings. You can also pause or restart directly from the notification, which pauses or restarts workout in service and StopwatchFragment. Icon also changes based on workout type.

I have added a feature which reminds you of every kilometer or mile you have achived in a workout by vibrating your phone for two seconds.

Pace calculation has also been changed, so in StopwatchFragment you can view it realtime, as it is calculated from last three positions. Speed calculations have also been rewritten as getting them directly from Location object has proven to be very inaccurate, so now it is calculated from last 3 locations. Altitude has been added to GpsPoint. So you can see how high you are and compare your speed  or pace to the altitude.

Add additional informations to StopwatchFragment has been added, so you can see your altitude, current speed and current pace.

In StopwatchFragment is a button which allows you to see statistics of your workout in a form of graphs. In GraphActivity I have implemented 3 graphs which show you speed, altitude and pace so you can analyze how well you have bee doing during workout.
